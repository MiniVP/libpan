![MiniVP Pan](img/banner.png)

`libpan` provides a base class with common code for all Pan weather stations.  
Consumers should write a class that extends the `Pan` class and provide two methods:

* `void begin()`: Initialize components.
* `String buildOutput()`: Return a comma-separated list of sensor values.

## Example

``` cpp
class PanDemo: public Pan {
  public:
    PanDemo(): Pan() {
    }
    void begin() {
      // Initialize the base class
      Pan::begin();
      // Initialize anything else
      someSensor.begin();
    }
    String buildOutput() {
      // Get base output from base class (for example, the timestamp)
      String out = Pan::buildOutput();
      // Add sensor outputs
      out.concat(someSensor.getOutput1());
      // Separate other outputs with commas
      out.concat(F(","));
      out.concat(someSensor.getOutput2());
      return out;
    }
}

// Arduino code

PanDemo panDemo;

void setup() {
  panDemo.begin();
}

void loop() {
  panDemo.loop();
}
```

## Software requirements

It is recommended to use [PlatformIO](https://platformio.org/) to use this library. However, nothing is truly required; do whatever you want!

## Build flags

Name             | Default value    | Description
---------------- | ---------------- | -----------
`DEBUG`          | 1                | Enables debug mode; outside of the serial control mode, the station will print debugging information to a serial link.
`USE_SD`         | 1                | Enable SD card support.
`USE_RTC`        | 1                | Enable real-time clock support.
`USE_LOWPOWER`   | 0                | Use the `LowPower` library to idle between measurements while drawing less power.
`USE_JUMPER`     | 1                | Enable serial control, activated at station startup using a jumper.
`FORCE_JUMPER`   | undefined        | Force serial control and ignore the jumper. Useless is `USE_JUMPER` is not enabled.
`FREQUENCY`      | 300000           | Duration, in milliseconds, between each measurement.
`STATION_ID`     | `PAN-xx-A-1-300` | A unique station identifier. MiniVP's convention is `ModelName-ModelType-Revision-SerialNo-Frequency`, with the frequency in seconds.
`LED_PIN`        | 9                | The digital pin for the LED indicator. Must be an output pin.
`SD_CS_PIN`      | 4                | The SPI Slave Select pin, or CS, for the SD card. Must be an output pin.
`JUMPER_IN_PIN`  | 2                | The digital input pin for the serial control jumper.
`JUMPER_OUT_PIN` | 3                | The digital output pin for the serial control jumper.

## Troubleshooting

This base class provides troubleshooting using a status LED for RTC and SD connection.

* **The LED turns on, then blinks twice in a loop**: The real-time clock reports an incorrect status. Check the RTC is properly connected and its date/time information is properly set.
* **The LED turns on, then blinks 4 times in a loop**: The SD card initialization failed. Check the pinout and connection to the SD card reader and check the SD card is properly plugged in and works on another device.
* **The LED turns on, then blinks 8 times in a loop**: The SD card was properly initialized but the `DATA.CSV` file could not be opened. Check that the card or file is not read-only, that there is still some disk space on the card, and that the card is properly formatted in FAT16 or FAT32 format as per the [Arduino recommendations](https://www.arduino.cc/en/Reference/SDCardNotes).
* **The LED turns on and does not turn off**:
  * The Arduino is stuck while trying to initialize either the real-time clock or the SD card. Check their pinouts and connections and try again.
  * The Arduino detected the serial control jumper and is now waiting for a serial connection for commands.
    The LED will, in this case, never turn off even after connecting.

By enabling the `DEBUG` preprocessor directive, you will be able to get logs sent back to you, particularly during the initialization process, that may be useful for troubleshooting the board.

## Controlling via serial

By enabling the `USE_JUMPER` preprocessor directive, you can let the station enter a special "remote control" mode over serial when booting it with a wire between D2 and D3 (`JUMPER_IN_PIN` and `JUMPER_OUT_PIN`).

Once done, open a serial connection to the Arduino (using the Arduino IDE's serial monitor, or `screen`, `minicom`, `picocom`, `platformio device monitor`, Visual Studio Code's Arduino extension, `echo` and `cat`, HyperTerminal, PuTTY, etc.). You can then send commands to the station:

* `PING` : A simple test command that should send `PONG` back.
* `ID` : Will return the station's ID, formatted as `PAN-HT-X-Y-Z`:
  * `X` is the revision number (currently, revision A) ;
  * `Y` is the unique serial number of the station ;
  * `Z` is the frequency, in seconds, of data gathering. For Pan-UV, it defaults to 300.
  Example : `PAN-HT-A-42-600` is a Pan-HT weather station revision A with serial number 42 and collecting data every 10 minutes.
* `GET` : Get a single line of CSV output, just like what would be saved by the station when running normally.

If the firmware was compiled with the `USE_RTC` directive, the following commands are also available:

* `TIMESTAMP` : Get a UNIX timestamp or a ISO 8601 date/time without timezones (`yyyy-mm-ddThh:mm:ss`) for the currently set date/time on the station ;
* `DATE <YYYY> <MM> <DD>` : Allows to set the current date on the station's real-time clock. For example, `DATE 2001 09 11` will set the date to September 11th, 2001 ;
* `TIME <HH> <MM> <SS>` : Allows to set the current time on the station's real-time clock, in 24-hour format. For example, `TIME 06 09 42` will set the time to 6:09:42 AM.

If the firmware was compiled with the `USE_SD` directive, the following commands are also available:

* `HARVEST` : Get all the records that were stored on the SD card, without altering it ;
* `CLEAR` : Remove all the stored records from the SD card.
